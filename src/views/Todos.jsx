import React from 'react';
import { Link } from 'react-router-dom';

export class Todos extends React.Component {
  renderItem = ({ id, title }) => (
    <div key={id}>
      <Link to={`/todos/${id}`}>{title}</Link>
    </div>
  );

  render() {
    const { todos } = this.props;
    if (!todos) {
      return <div className="loader" />;
    }

    return <div>{todos.map(item => this.renderItem(item))}</div>;
  }
}
