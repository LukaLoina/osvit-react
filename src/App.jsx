import React, { Component } from 'react';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import { ErrorPage, Todos, TodosDetails, LifecycleMethods } from './views';
import { AppLayout } from './components';
import './css/App.css';

export class App extends Component {
  state = {
    todos: undefined
  };

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(todos => this.setState({ todos }))
      .catch(error => console.log(error));
  }

  renderRoute = props => <Todos {...props} {...this.state} />;

  render() {
    return (
      <BrowserRouter>
        <AppLayout>
          <Switch>
            <Redirect exact from="/" to="/todos" />
            <Route exact path="/todos" render={this.renderRoute} />
            <Route path="/todos/:id" render={this.renderRoute} />
            <Route path="/lifecycle" component={LifecycleMethods} />
            <Route component={ErrorPage} />
          </Switch>
        </AppLayout>
      </BrowserRouter>
    );
  }
}
